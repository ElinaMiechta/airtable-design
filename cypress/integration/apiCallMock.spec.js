describe('cypress test for Mocking Api Request', () => {
  it('should increase number of votes', () => {
    cy.visit('/')
    cy.intercept('GET', `**/v0/${Cypress.env('BASE_ID')}/Survey*`, {
      records: [],
    })

    cy.intercept('GET', `**/v0/${Cypress.env('BASE_ID')}/Survey*`, {
      fixture: 'survey.json',
    })

    cy.get('section').contains('Survey')
    cy.wait(500)
    cy.get('.container')
      .find('ul')
      .find('li')
      .find('div p')
      .then(li => {
        const votes1 = li[0]
        expect(votes1).to.contain('6')
        const votes2 = li[1]
        expect(votes2).to.contain('20')
      })

    cy.fixture('survey').then(file => {
      //   const updatedVote = file.records[0].fields.votes
      cy.intercept('PATCH', `**/v0/${Cypress.env('BASE_ID')}/Survey/*`, {
        fixture: 'survey.json',
      })
    })

    cy.contains('Survey')
    cy.wait(500)
    cy.get('.container ul').find('li').eq(0).find('button').click()
    cy.wait(500)

    cy.get('.container ul')
      .find('li')
      .eq(0)
      .find('div p')
      .should('contain', '7')
  })
})
