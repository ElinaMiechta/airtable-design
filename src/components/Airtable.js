import Airtable from 'airtable'
//this Airtable is happening at the runtime ✌️
export default new Airtable({ apiKey: process.env.GATSBY_AIRTABLE_API }).base(
  process.env.GATSBY_AIRTABLE_BASE_ID
)
